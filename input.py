import pygame
from pygame.locals import *


class EventHandler():
    """Handles events received from mouse and keyboard, no joystick support yet"""
    _key_up = {}
    _key_down = {}
    _mouse_down = {}
    _mouse_up = {}
    _mouse_events = [MOUSEBUTTONDOWN, MOUSEBUTTONUP]
    _keyboard_events = [KEYDOWN, KEYUP]

    def __init__(self):
        self.assign_keyup(K_q, self.quit)
        self.assign_keyup(K_ESCAPE, self.quit)

    def assign_keyup(self, key, callback, *args, **kwargs):
        """assigns a callback to keyup event"""
        assert callable(callback), \
            'callback must be callable value'
        self._key_up[key] = (callback, args, kwargs)

    def assign_keydown(self, key, callback, *args, **kwargs):
        """assigns a callback to keydown event"""
        assert callable(callback), \
            'callback must be callable value'
        self._key_down[key] = (callback, args, kwargs)


    def assign_mousedown(self, button, callback, *args, **kwargs):
        """assigns a callback to mouse button down event"""
        assert callable(callback), \
            'callback must be a callable value'
        self._mouse_down[button] = (callback, args, kwargs)

    def assign_mouseup(self, button, callback, *args, **kwargs):
        """assigns a callback to mouse button up event"""
        assert callable(callback), \
            'callback must be a callable value'
        self._mouse_up[button] = (callback, args, kwargs)

    def handle_events(self):
        """calls necessary callbacks when events occur"""
        #e = pygame.event.poll()
        for e in pygame.event.get():
            if e.type == QUIT:
                quit()
            elif e.type in self._mouse_events:
                self.__mouse__(e)
            elif e.type in self._keyboard_events:
                self.__keyboard__(e)

    def __mouse__(self, e):
        """handles all mouse events"""
        mouse_down = self._mouse_down
        mouse_up = self._mouse_up
        if e.type == MOUSEBUTTONDOWN:
            if e.button in mouse_down:
                f, args, kwargs = mouse_down[e.button]
                f(*args, **kwargs)
        elif e.type == MOUSEBUTTONUP:
            if e.button in mouse_up:
                f, args, kwargs = mouse_up[e.button]
                f(*args, **kwargs)

    def __keyboard__(self, e):
        """handles all keyboard events"""
        key_up = self._key_up
        key_down = self._key_down
        if e.type == KEYDOWN:
            if e.key in key_down:
                f, args, kwargs = key_down[e.key]
                f(*args, **kwargs)
        elif e.type == KEYUP:
            if e.key in key_up:
                f, args, kwargs = key_up[e.key]
                f(*args, **kwargs)

    def quit(self):
        """quit pygame and terminate program"""
        pygame.quit()
        exit(0)

