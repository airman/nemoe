import random
import pygame
from . import bg_scrollable
from .gameobject import GameObject
from .ui import UIObject


class BaseRoom(pygame.sprite.Sprite):
    """
        Base class used to represent a screen state
    """
    _WIGGLE = 5

    def __init__(self, background, size):
        super(BaseRoom, self).__init__()
        self.background = background
        self._all_sprites = pygame.sprite.Group()
        self.rect = pygame.Rect(0, 0, *size)
        self.image = pygame.Surface((size[0] + BaseRoom._WIGGLE,
                                     size[1] + BaseRoom._WIGGLE))
        self._shake = False

    def __shake__(self):
        """
            called by update method to shake room if 
            self._shake is True
        """
        self.rect.x = random.randint(-BaseRoom._WIGGLE, 0)
        self.rect.y = random.randint(-BaseRoom._WIGGLE, 0)

    def toggle_shake(self, b=None):
        """Toggles room shake"""
        self._shake = b if b is not None else not self._shake
        if not self._shake:
            self.rect.x, self.rect.y = 0, 0

    def add_sprite(self, spr):
        """Add GameObject to be drawn"""
        if isinstance(spr, GameObject):
            self._all_sprites.add(spr)
            spr._rooms.add(self)

    def remove(self, spr):
        """Remove sprite from room"""
        self._all_sprites.remove(spr)


class MenuRoom(BaseRoom):

    """
        Used to create a Room in which a menu will be
        displayed using UIObjects.  
    """
    def __init__(self, background, size):
        super(MenuRoom, self).__init__(background, size)

    def add_ui_object(self, obj):
        """Add UI object to MenuRoom"""
        if isinstance(obj, UIObject):
            self_all_sprites.add(obj)

    def update(self, mouse_pos, surf):
        image = self.image
        image.blit(self.background, (0, 0))
        self._all_sprites.update(mouse_pos, image)
        if self._shake:
            self.__shake__()
        surf.blit(image, self.rect)

class GameRoom(BaseRoom):
    """
        Used to represent level screen.Stores 
        all level sprites, handles collision 
        checks.
    """

    def __init__(self, background, size):
        super(GameRoom, self).__init__(background, size)
        self._scroll_speed = 1
        self._bg_scroll_speed = 1
        if background is None:
            pass
        elif isinstance(background, pygame.Surface):
            self._scrollable = False
        elif isinstance(background, bg_scrollable.BGScrollable):
            self._scrollable = True
            background.set_scroll_val(self._scroll_speed)
        self.enemy_sprites = pygame.sprite.Group()  # enemy sprites
        self.player_sprites = pygame.sprite.Group()  # player sprites
        self.obstacles = pygame.sprite.Group()  # objects in room,
        self.background_objects = pygame.sprite.Group()  # non-collideable
        self.non_player = pygame.sprite.Group()
        self._position = [0,0]
        self._player_last_checkpoint = None
        self._check_points = []
        
    def set_scroll_speed(self, val):
        """set room scroll speed"""
        self._scroll_speed = val

    def get_scroll_speed(self):
        return self._scroll_speed

    def set_bg_scroll_speed(self, val):
        """Only set background scroll speed, this does not affect scroll speed
        of non-background objects"""
        if isinstance(self.background, bg_scrollable.BGScrollable):
            self.background.set_scroll_val(val)
        self._bg_scroll_speed = val
        
    def get_bg_scroll_speed(self):
        return self._bg_scroll_speed

    def add_background_object(self, spr):
        """Add a GameObject to background, (non-collideable objects)"""
        if isinstance(spr, GameObject):
            self.background_objects.add(spr)
            self.add_sprite(spr)
            self.non_player.add(spr)

    def add_obstacle(self, spr):
        """Add a GameObject as collideable obstacle"""
        if isinstance(spr, GameObject):
            self.obstacles.add(spr)
            self.add_sprite(spr)
            self.non_player.add(spr)

    def add_enemy(self, spr):
        """Add a GameObject as enemy"""
        if isinstance(spr, GameObject):
            self.enemy_sprites.add(spr)
            self.add_sprite(spr)
            self.non_player.add(spr)

    def add_player(self, spr):
        """Add a GameObject as player"""
        if isinstance(spr, GameObject):
            self.player_sprites.add(spr)
            self.add_sprite(spr)

    def add_positional_check_point(self, x):
        """once player passes this point, current player objects are saved"""
        self._check_points.append(x)  
        
    def remove(self, spr):
        """Remove GameObject from remove"""
        self.player_sprites.remove(spr)
        self.obstacles.remove(spr)
        self.enemy_sprites.remove(spr)
        self.background_objects.remove(spr)
        self.non_player.remove(spr)
        super(AnimGameObject, self).remove_sprite(spr)

    def scroll_forward(self):
        """scroll background and non-player sprites forward"""
        if self._scrollable:
            self.background.scroll_forward()
        speed = self._scroll_speed
        bg_speed = self._bg_scroll_speed
        for spr in self.non_player:
            if spr in self.background_objects:
                spr.set_position(spr.rect.x - bg_speed, spr.rect.y)
            else:
                spr.set_position(spr.rect.x - speed, spr.rect.y)
        self._position[0] += speed

    def scroll_backward(self):
        """scroll background and non-player sprites backward"""
        if self._scrollable:
            self.background.scroll_backward()
        speed = self._scroll_speed
        bg_speed = self._bg_scroll_speed
        for spr in self.non_player:
            if spr in self.background_objects:
                spr.set_position(spr.rect.x + bg_speed, spr.rect.y)
            else:
                spr.set_position(spr.rect.x + speed, spr.rect.y)
        self._position[0] -= speed

    def update(self, dt, surf):
        image = self.image
        if self.background is not None:
            if self._scrollable:
                self.background.render(image)
            else:
                image.blit(self.background, (0, 0))
        self.background_objects.update(dt, image)
        self.obstacles.update(dt, image)
        self.player_sprites.update(dt, image)
        if self._shake:
            self.__shake__()
        surf.blit(image, self.rect)

    def save(self):
        pass

