import pygame

__doc__ = """
    This module contains classes to be used in creating
    scrollable backgrounds based on a single repeated image.
    
    This is suitable only for negligable backgrounds whose
    sole purpose is to set the scene and convey vertical or 
    horizontal motion. 

    This is not recommended in cases where the 
    background holds significant value, such as tile based 
    games.
"""


class BGScrollable(object):
    """
        A class for a continuously moving background
        using 1 repeatable background image 
    """
    _WIGGLE = 5  # used when shaking the screen

    def __init__(self, bg, size, hz=True):

        bg_size = bg.get_size()
        if hz:
            self._bg = pygame.transform.scale(bg, (bg_size[0] + BGScrollable._WIGGLE, size[1] + BGScrollable._WIGGLE))
            p2 = [bg_size[0], 0]
        else:
            self._bg = pygame.transform.scale(bg, (size[0] + BGScrollable._WIGGLE, bg_size[1] + BGScrollable._WIGGLE))
            p2 = [0, bg_size[1]]
        self._pos = [[0, 0], p2]
        self._size = size
        self._curr = 0
        self._othr = 1
        self._scroll_val = 0
        self._surf = pygame.Surface(size)

    def __scroll__(self, delta, indx, lim):
        """
            Capable of scrolling background vertically or horizontally
            used by scroll_<direction> methods, don't invoke directly
        """
        scroll = self._scroll_val
        pos = self._pos
        pos[0][indx] += delta
        pos[1][indx] += delta
        curr = self._curr
        othr = self._othr
        # moving forward
        if pos[curr][indx] <= -lim:
            diff = pos[curr][indx] + lim
            pos[curr][indx] = lim - scroll - diff
            self._curr, self._othr = self._othr, self._curr
        # moving backward
        elif pos[othr][indx] > lim:
            diff = pos[othr][indx] - lim
            pos[othr][indx] = -lim + diff + scroll
            self._curr, self._othr = self._othr, self._curr

    def render(self, _surf):
        """renders the resultant image onto the given surface"""
        pos = self._pos
        bg = self._bg
        _surf.fill((255, 255, 255))
        _surf.blit(bg, pos[0])
        _surf.blit(bg, pos[1])

    def scroll_forward(self):
        """Scroll image forward"""
        pass

    def scroll_backward(self):
        """Scroll image backward"""
        pass

    def set_scroll_val(self, val):
        """set value to shift background by"""
        self._scroll_val = val

    def get_surf(self):
        """returns the resulting image surface"""
        _surf = self._surf
        pos = self._pos
        bg = self._bg
        _surf.fill((255, 255, 255))
        _surf.blit(bg, pos[0])
        _surf.blit(bg, pos[1])
        return _surf


class BGHScrollable(BGScrollable):
    """Horizontally scrollable background"""

    def __init__(self, bg, size):
        super(BGHScrollable, self).__init__(bg, size, True)

    def scroll_forward(self):
        w, h = self._bg.get_size()
        w -= BGScrollable._WIGGLE
        self.__scroll__(-self._scroll_val, 0, w)

    def scroll_backward(self):
        w, h = self._bg.get_size()
        w -= BGScrollable._WIGGLE
        self.__scroll__(self._scroll_val, 0, w)


class BGVScrollable(BGScrollable):
    """Vertically scrollable background"""

    def __init__(self, bg, size):
        super(BGVScrollable, self).__init__(bg, size, False)
        self._pos = [[0, 0], [0, bg.get_size()[1]]]

    def scroll_backward(self):
        w, h = self._bg.get_size()
        h -= BGScrollable._WIGGLE
        self.__scroll__(self._scroll_val, 1, h)

    def scroll_forward(self):
        w, h = self._bg.get_size()
        h -= BGScrollable._WIGGLE
        self.__scroll__(-self._scroll_val, 1, h)
