
import sys

#sys.path.insert(0, '../')

import pygame
from pygame import Color
from nemoe import ui
from nemoe.input import EventHandler

pygame.init()
HEIGHT = 480
WIDTH = 640

def hello():
    print('hello world')

def quit():
    pygame.quit()
    exit(0)

def main():

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    clock = pygame.time.Clock()
    events = EventHandler()

    btnGroup = ui.ButtonGroup((200, 200))
    btnGroup.set_button_property('background_color', (0, 0, 0))
    btnGroup.set_button_property('border_color', (145, 0, 0) )

    btnGroup.add_button('hello', hello)
    btnGroup.add_button('bye', quit)
    btnGroup.center(screen.get_size())
    events.assign_mouseup(1, btnGroup.event_callback)

    while True:
        clock.tick(50)
        screen.fill((80,80,80))
        events.handle_events()

        mouse_pos = pygame.mouse.get_pos()
        btnGroup.update(mouse_pos, screen)
        pygame.display.update()

if __name__ == '__main__':
    main()
