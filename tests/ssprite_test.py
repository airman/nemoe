
import sys

#sys.path.insert(0, '../')

import pygame, time
from pygame import Color
from pygame.sprite import Group
from nemoe.ssprite import *
from nemoe.strip import *
from nemoe.utils import slice_hsurf, slice_msurf
from nemoe.input import EventHandler

HEIGHT = 480
WIDTH = 640
pygame.init()

def main():

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    clock = pygame.time.Clock()
    events = EventHandler()

    
    frog = pygame.image.load('./data/images/frog_leap.png').convert_alpha()
    frog_frames = slice_hsurf(frog, width=50, height=67)
    
    naruto = pygame.image.load('./data/images/naruto_run.png').convert_alpha()
    naruto_frames = slice_hsurf(naruto, width=51)
    anim_naruto = SSprite(naruto_frames[0], frame_order=STRIP_PINGPONG)
    anim_naruto.add_strip('run', naruto_frames[1:], repeat=-1, strip_timing=0.1)
    
    anim_frog = SSprite(frog_frames, strip_timing=0.27)
    anim_frog.rect.topleft = (0,50)
    spr_group =  Group(anim_naruto, anim_frog)
    move = [False, False]

    def moveLeft(m):
        m[0] = True
        m[1] = False
        anim_naruto.set_strip('run')

    def moveRight(m):
        m[0] = False
        m[1] = True
        anim_naruto.set_strip('run')

    def moveNot(m):
        m[0] = False
        m[1] = False
        anim_naruto.set_strip('default')

    
    events.assign_keydown(pygame.K_RIGHT, moveLeft, move)
    events.assign_keydown(pygame.K_LEFT, moveRight, move)
    events.assign_keyup(pygame.K_RIGHT, moveNot, move)
    events.assign_keyup(pygame.K_LEFT, moveNot, move)
    t = time.time()
    while True:
        clock.tick(50)
        screen.fill(Color('black'))
        events.handle_events()

        if move[0]:
            anim_naruto.rect.left += 3

        elif move[1]:
            anim_naruto.rect.left -= 3
        spr_group.update(time.time() - t, screen)
        pygame.display.update()


if __name__ == '__main__':
    main()
