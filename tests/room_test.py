
import sys

#sys.path.insert(0, '../')

import time
import pygame
from pygame import Color
from pygame.locals import *

from nemoe import room
from nemoe import gameobject
from nemoe import resources as res

from nemoe.utils import slice_hsurf
from nemoe.bg_scrollable import BGHScrollable
from nemoe.input import EventHandler

pygame.init()
HEIGHT = 480
WIDTH = 640
data = {}

speed= 1
def prep_data():
    res.load_images()
    img = res.GFX['guy_walk']
    w,h = img.get_size()
    img = pygame.transform.scale(img, (w*3, h*3))
    naruto_frames = slice_hsurf(img, 90)
    anim_naruto = gameobject.AnimGameObject(naruto_frames[0])
    anim_naruto.add_strip('run', naruto_frames[1:], repeat=-1, strip_timing=0.15)
    anim_naruto.set_position(0, HEIGHT - anim_naruto.rect.height)
    frog_frames = slice_hsurf(res.GFX['frog_leap'], width=50, height=67)
    frog = gameobject.GameObject(frog_frames[0])
    frog.set_position(WIDTH, HEIGHT - frog.rect.height)
    bg = BGHScrollable(res.GFX['bg'], (WIDTH, HEIGHT))
    r = room.GameRoom(bg, (WIDTH, HEIGHT))
    r.add_player(anim_naruto)
    r.add_background_object(frog)
    data['room'] = r            
    data['player'] = anim_naruto
    data['time'] = time.time()
    data['events'] = EventHandler()
    data['player_move'] = None
    data['frog'] = frog

def player_move_right():
    data['player_move'] = 'right'
    data['player'].set_velocity(speed, 0)
    data['player'].set_strip('run')

def player_move_left():
    data['player_move'] = 'left'
    data['player'].set_velocity(-speed, 0)
    data['player'].set_strip('run')

def player_stop():
    data['player_move'] = None
    data['player'].set_velocity(0, 0)
    data['player'].set_strip('default')

def shake():
    data['room'].toggle_shake()

def assign_keys():
    e = data['events']
    e.assign_keydown(K_j, player_move_left)
    e.assign_keydown(K_l, player_move_right)
    e.assign_keyup(K_s, shake)
    e.assign_keyup(K_j, player_stop)
    e.assign_keyup(K_l, player_stop)

def main():
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    clock = pygame.time.Clock()
    prep_data()
    assign_keys()
    events = data['events']
    while True:
        events.handle_events()
        clock.tick(50)
        screen.fill(Color('white'))
 
        if data['player_move'] == 'left':
            data['room'].scroll_backward()
        elif data['player_move'] == 'right':
            data['room'].scroll_forward()

        if data['player'].rect.left > WIDTH/2 and data['player_move'] == 'right':
            data['player'].set_velocity(0,0)
            data['room'].set_bg_scroll_speed(2)
        elif data['player'].rect.left < 3 and data['player_move'] == 'left':
            data['player'].set_velocity(0,0)
            data['room'].set_bg_scroll_speed(2)
        else:
            data['room'].set_bg_scroll_speed(1)

        dt = time.time() - data['time']
        data['room'].update(dt, screen)
        pygame.display.update()
        pygame.display.set_caption('FPS: %s' % round(clock.get_fps()))
        
if __name__ == '__main__':
    main()
