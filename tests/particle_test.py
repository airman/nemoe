__doc__ = """
    This is a playground to test the particle class and it's related
    components as well as some concepts to their usage
    

    KEYS:
        a -> create smoke spawner, follows mouse pos 
        mouse click -> create explosion 
        keypad_+ -> increase smoke spawner intensity
        keypad_-+ -> decrease smoke spawner intensity

"""
import sys

#sys.path.insert(0, '../')

import math, time, random
import pygame
from pygame import Color
from nemoe.vec2d import Vec2d

from nemoe import particle

pygame.init()
HEIGHT = 480
WIDTH = 640


def main():

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    clock = pygame.time.Clock()
    t = time.time()
    colors = (44, 91, 216), (216, 102, 44),(216, 44, 54), (216, 102, 44),\
        (216, 216, 44), (145, 216, 44)
    particles = pygame.sprite.Group()
    spawners = pygame.sprite.Group()
    auto = False
    elapsed = 0
    sm = None
    while True:
        e = pygame.event.poll()
        clock.tick(50)
        screen.fill(Color('black'))

        if e.type == pygame.QUIT:
            pygame.quit()
            break
        elif e.type == pygame.KEYUP:
            if e.key in (pygame.K_ESCAPE, pygame.K_q):
                pygame.quit()
                break
            elif e.key == pygame.K_KP_PLUS:
                if sm is not None:
                    sm.set_intensity(sm._intensity + 5)
            elif e.key == pygame.K_KP_MINUS:
                if sm is not None:
                    sm.set_intensity(sm._intensity - 5)
            elif e.key == pygame.K_a:
                if sm is not None:
                    spawners.remove(sm)
                sm = particle.Smoke(pygame.mouse.get_pos(), particles, life_span=50, intensity=10, spawn_time=0.1)
                # sm.set_particle_
                # sm.set_particle_property('image', particle.explosion_block)
                # sm.set_particle_property('reproduce', True)
                # sm.set_particle_class(particle.CyclicParticle)
                spawners.add(sm)
            elif e.key == pygame.K_k:
                if sm is not None:
                    sm.kill()
        elif e.type == pygame.MOUSEBUTTONDOWN:
            m_pos = pygame.mouse.get_pos()
            fireworks = particle.RadialSplatter(m_pos, particles, life_span=0.3)
            fireworks.set_particle_property('reproduce', True)
            fireworks.set_particle_property('max_generations', 1)
            fireworks.add_particle_color(*colors)
            fireworks.toggle_colorize()
            spawners.add(fireworks)

        dt = time.time() - t
        particles.update(dt + t, screen)
        spawners.update(dt + t)
        if sm is not None:
            pos = list(pygame.mouse.get_pos())
            sm.set_pos(pos)
        pygame.display.flip()
        pygame.display.set_caption('FPS: %s' % round(clock.get_fps()))

if __name__ == '__main__':
    main()
