from .vec2d import Vec2d
from .ssprite import SSprite
import pygame


class GameObject(pygame.sprite.Sprite):

    def __init__(self, image):
        super(GameObject, self).__init__()
        # if image != None:
        self.image = image
        self.rect = image.get_rect()
        self._orig_image = self.image.copy() # stores copy of original image
        self._velocity = Vec2d(0, 0)
        self._position = Vec2d(0, 0)
        self._airborne = False
        self._airtime = 0
        self._rooms = set()
        self._gravity = Vec2d(0, 0.8)

    def __gravity__(self, dt):
        """Apply gravity to object"""
        t = dt - self._airtime
        self._velocity += self._gravity * t

    def __collide__(self, other):
        """method to call if GameObject collides with another GameObject"""
        pass

    def __move__(self): 
        """Called by update method to change object's position using object's velocity"""
        v = self._velocity
        p = self._position
        p += v
        self.rect.x = round(p.x)
        self.rect.y = round(p.y)

    def set_gravity(self, x, y):
        """Set GameObject gravity, used by all GameObjects"""
        self._gravity.x = x
        self._gravity.y = y

    def add_to_room(self, room):
        """Add object to Room"""
        room.add_sprite(self)

    def toggle_airborne(self, b=None):
        """toggle self._airborne, if true, gravity will be active"""
        self._airborne = b if b != None else not self._airborne
        if not self._airborne:
            self._airtime = 0

    def set_velocity(self, x, y):
        """Set object's velocity"""
        self._velocity.x = x
        self._velocity.y = y

    def set_position(self, x, y):
        """Set the position of the object"""
        self._position.x = x
        self._position.y = y
        self.rect.topleft = x, y

    def get_position(self):
        """Get the position of the object"""
        return tuple(self._position)

    def update(self, dt, surf):
        if self._airborne:
            if self._airtime == 0:
                self._airtime = dt
            self.__gravity__(dt)
        self.__move__()
        surf.blit(self.image, self.rect)


class AnimGameObject(GameObject, SSprite):
    def __init__(self, default_frames, **kwargs):
        SSprite.__init__(self, default_frames, **kwargs)
        self._velocity = Vec2d(0, 0)
        self._position = Vec2d(0, 0)
        self._airtime = 0
        self._airborne = False
        self._rooms = set()

    def update(self, dt, surf):
        if self._airborne:
            if self._airtime == 0:
                self._airtime = dt
            self.__gravity__(dt)
        self.__move__()
        SSprite.update(self, dt, surf)

