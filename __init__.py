
__doc__="""
    NEMOE
        Not Exactly Much Of an Engine

        This is a library of classes meant to help facilitate
        the creation of games using python and pygame.

        This does not restrict you to conform to any of the library's
        standards if any, but rather pick and choose what you'd like
        to make use of.
        
        Though it would be recommended that displayable objects inherit
        from the GameObject class in order to use the Room class.

"""
