import os
import pygame
from .utils import slice_hsurf, slice_msurf

__doc__ = \
    """
	This module contains functions for loading and storing
	media resources. 
	
	load_<name> functions use values in 'res_data' dictionary
	to load files using path and list of extensions. The values
	in the 'res_data' variable should be adjusted using the
	set_<media>_<var> functions. Where <media> is either 'sound' 
	or 'image' and <var> is either 'path' or 'extensions'

	e.g set_sound_path(path) 
		set_sound_extensions(path)

	All loaded sounds are stored in 'SFX' dictionary,
	with the filename(without extension) as the key.
	
	All loaded images are stored in 'GFX' dictionary,
	with the filename(without extension) as the key.

	default values for res_data:

		res_data['sound'] = [os.path.join('data', 'sounds'), ('.ogg', '.wav')]
		res_data['image'] = [os.path.join('data', 'images'), ('.png', '.jpg', '.jpeg')]
"""

# default resource directories
res_data = {}
res_data['sound'] = [os.path.join('data', 'sounds'), ('.ogg', '.wav')]
res_data['image'] = [os.path.join('data', 'images'), ('.png', '.jpg', '.jpeg')]

SFX = {}
GFX = {}


def set_sound_path(path):
    res_data['sound'][0] = path


def set_image_path(path):
    res_data['image'][0] = path


def set_image_extensions(ext_list):
    res_data['image'][1] = ext_list


def set_sound_extensions(ext_list):
    res_data['sound'][1] = ext_list


def get_file_list(res_data, ext_list):
    file_list = os.listdir(res_data)
    file_names = []
    for f in file_list:
        ext = os.path.splitext(f)[1]
        if ext in ext_list:
            name = os.path.join(res_data, f)
            file_names.append(os.path.abspath(name))

    return file_names


def load_all_resources():
    load_sounds()
    load_images()


def load_sounds():
    """loads sound resources using 'res_data' dictionary"""
    for f in get_file_list(*res_data['sound']):
        name = os.path.splitext(os.path.basename(f))[0]
        SFX[name] = pygame.mixer.Sound(f)


def load_images():
    """loads sound resources using 'res_data' dictionary"""
    for f in get_file_list(*res_data['image']):
        name = os.path.splitext(os.path.basename(f))[0]
        GFX[name] = pygame.image.load(f).convert_alpha()
